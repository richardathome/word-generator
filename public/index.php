<?php

use Richbuilds\WordGenerator\WordModel;

require_once __DIR__ . '/../vendor/autoload.php';

$nm = new WordModel(['thorin','balin','dwalin', 'fili', 'kili', 'oin', 'gloin', 'nori', 'dori', 'ori', 'bifur', 'bofur', 'bombur']);
//$nm = new NameModel(['Doc', 'Sleepy','Dopey','Grumpy','Happy','Bashful','Sneezy']);

for ($i = 0; $i < 10; $i++) {
    echo ucfirst($nm->generate()) . '<br/>';
}