<?php
declare(strict_types=1);

namespace Richbuilds\WordGenerator;

use Exception;

/**
 *
 */
class WordModel
{
    public const END_OF_WORD = 'END_OF_WORD';

    /**
     * Stores the chance of a letter being a starting letter
     *
     * @var array{string,int}
     */
    private array $starting_letter_chances = [];

    /**
     * Stores the chance of a letter following another letter
     *
     * @var array{string,array{int,string}}
     */
    private array $next_letter_chances = [];

    /**
     * @param array{string} $words
     *
     * @return void
     */
    public function __construct(array $words = [])
    {
        $this->train($words);
    }

    /**
     * Trains the model.
     *
     * Records starting letter chances and next letter chances
     *
     * @param array{string} $words
     *
     * @return void
     */
    private function train(array $words = []): void
    {
        $this->starting_letter_chances = [];
        $this->next_letter_chances = [];

        if (empty($words)) {
            return;
        }

        foreach ($words as $word) {
            $word = strtolower($word);

            // score starting letter
            $previous_letter = $word[0];
            $this->starting_letter_chances[$previous_letter]++;

            // score remaining letters
            for ($i = 1; $i < strlen($word); $i++) {
                $current_letter = $word[$i];
                $this->next_letter_chances[$previous_letter][$current_letter]++;
                $previous_letter = $current_letter;
            }

            $this->next_letter_chances[$previous_letter][self::END_OF_WORD]++;
        }

        // sort the letter lists, the highest scoring letters first
        asort($this->starting_letter_chances);

        foreach ($this->next_letter_chances as &$chances) {
            asort($chances);
        }

    }

    /**
     * Returns the current list of starting letter chances
     *
     * @return array{string,int}
     */
    public function getStartingLetterChances(): array
    {
        return $this->starting_letter_chances;
    }

    /**
     * Get the chance of $second letter following $first
     * Only used for testing
     *
     * @param string $first
     * @param string $second
     *
     * @return int
     */
    public function getNextLetterChance(string $first, string $second): int
    {
        return $this->next_letter_chances[$first][$second] ?? 0;
    }

    /**
     * Generates a word by first picking a starting letter and then picking
     * letters until and end of the word symbol '_' is reached
     *
     * @return string
     * @throws Exception
     */
    public function generate(): string
    {
        if (empty($this->starting_letter_chances)) {
            return '';
        }

        $word = '';

        $current_letter = $this->pickLetter($this->starting_letter_chances);

        while ($current_letter !== self::END_OF_WORD) {
            $word .= $current_letter;
            $current_letter = $this->pickLetter($this->next_letter_chances[$current_letter]);
        }

        return $word;
    }

    /**
     * Picks a random letter from the weighted letter list
     *
     * Higher scoring letters will tend to be picked more often than lower
     * scoring words
     *
     * @param array $chances {string,int}
     *
     * @return string
     *
     * @throws Exception
     */
    public function pickLetter(array $chances): string
    {
        $total_chances = array_sum($chances);

        $target = random_int(0, $total_chances - 1);

        $current = -1;

        foreach ($chances as $letter => $chance) {
            $current += $chance;

            if ($current >= $target) {
                return $letter;
            }
        }

        throw new Exception('should never reach here!');
    }

    /**
     * Returns the current next letter chance matrix
     * Used for testing
     *
     * @return array{string,array{string,int}}
     */
    public function getNextLetterChances(): array
    {
        return $this->next_letter_chances;
    }
}