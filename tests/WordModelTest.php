<?php
declare(strict_types=1);

namespace Richbuilds\WordGenerator\Test;

use Exception;
use Richbuilds\WordGenerator\WordModel;
use PHPUnit\Framework\TestCase;

/**
 *
 */
class WordModelTest extends TestCase
{

    private WordModel $nm;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->nm = new WordModel([
            'car', 'cat', 'bat'
        ]);
    }

    /**
     * @return void
     */
    public function testTrainRecordsStartingLetters(): void
    {
        $expected = ['b' => 1, 'c' => 2];

        self::assertEquals($expected, $this->nm->getStartingLetterChances());
    }

    /**
     * @return void
     */
    public function testTrainRecordsMatrix(): void
    {
        self::assertEquals(0, $this->nm->getNextLetterChance('a', 'a'));
        self::assertEquals(2, $this->nm->getNextLetterChance('t', WordModel::END_OF_WORD));
    }

    /**
     * @return void
     */
    public function testMatrixIsSorted(): void
    {
        $matrix = $this->nm->getNextLetterChances();

        self::assertSame(['a'=>2], $matrix['c']);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testPickOne(): void
    {

        self::assertNotEquals('x', $this->nm->pickLetter($this->nm->getNextLetterChances()['a']));

    }

    /**
     * @return void
     * @throws Exception
     */
    public function testGenerate(): void
    {
        $name = $this->nm->generate();

        self::assertNotEmpty($name);
    }

    /**
     * @throws Exception
     */
    public function testGenerateOnEmptyTrainingModelReturnsEmptyString(): void
    {

        $nm = new WordModel();

        self::assertEmpty($nm->generate());
    }
}
